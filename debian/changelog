r-cran-survey (4.4-2-2) unstable; urgency=medium

  * Team upload.
  * Convert to arch:any with dh-make-r, fixes: #1081117
  * Reorder sequence of d/control fields by cme (routine-update)
  * Set upstream metadata fields: Archive.

 -- Charles Plessy <plessy@debian.org>  Thu, 17 Oct 2024 08:51:13 +0900

r-cran-survey (4.4-2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 08 Aug 2024 08:08:04 +0900

r-cran-survey (4.2-1-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 28 Jun 2023 14:08:13 +0200

r-cran-survey (4.1-1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-mitools.

 -- Andreas Tille <tille@debian.org>  Tue, 17 Aug 2021 17:41:43 +0200

r-cran-survey (4.0-2) unstable; urgency=medium

  * Team upload.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 18 May 2020 09:01:04 +0200

r-cran-survey (4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sat, 04 Apr 2020 18:13:50 +0200

r-cran-survey (3.37-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Test-Depends: r-cran-aer

 -- Andreas Tille <tille@debian.org>  Mon, 27 Jan 2020 11:48:17 +0100

r-cran-survey (3.36-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Tue, 09 Jul 2019 08:16:47 +0200

r-cran-survey (3.35-1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 04 Feb 2019 10:47:46 +0100

r-cran-survey (3.35-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Thu, 10 Jan 2019 16:32:42 +0100

r-cran-survey (3.34-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Sun, 14 Oct 2018 08:50:43 +0200

r-cran-survey (3.33-2-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Sun, 03 Jun 2018 13:32:49 +0200

r-cran-survey (3.33-2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 16 Mar 2018 09:32:51 +0100

r-cran-survey (3.33-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Mon, 12 Mar 2018 15:47:38 +0100

r-cran-survey (3.32-1-2) unstable; urgency=medium

  * Team upload.
  * Add missing r-cran-mass to test dependencies.
  * Fix Vcs-Browser URL.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 06 Nov 2017 11:05:17 +0100

r-cran-survey (3.32-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.32-1
  * Add r-cran-sqlite to test depends.
  * Bump Standards-Version to 4.1.1.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 30 Sep 2017 09:59:37 +0200

r-cran-survey (3.31-5-1) unstable; urgency=medium

  * Initial release (closes: #850622)

 -- Andreas Tille <tille@debian.org>  Sun, 08 Jan 2017 17:03:04 +0100
